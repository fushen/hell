const path = require("path");

module.exports = {
    devServer: {
        open: true,
        hot: true,
        host: "127.0.0.1",
        port: 8090
    },
    configureWebpack: {
        resolve: {
            alias: {
                assets: "@/assets",
                components: "@/components",
                router: "@/router",
                store: "@/store",
                views: "@/views",
                plugins: "@/plugins",
            }
        }
    }
}