import axios from 'axios';
import { ElMessage } from 'element-plus';

export const instance = axios.create({
    baseURL: process.env.NODE_ENV === "development" ? "" : "/",
    timeout: 10000
});

instance.interceptors.response.use(function(response){
    console.log("接口请求成功: ______________", response.headers.url);
    return response;
}, function(error){
    // 错误状态码处理
    if(error.response){
        switch(error.response.status){
            case "403":
                ElMessage.error("权限验证失败，请刷新页面重新登录!");
                break;
            case "500":
                ElMessage.error("服务器端出错，请联系管理员!");
                break;
            case "404":
                ElMessage.error("服务器端出错，请联系管理员!");
                break;
            default:
                break;
        }
        return Promise.reject(error.response);
    }
    // 断网处理
    if (!window.navigator.onLine) {
        ElMessage.error("网络错误，请检查网络连接!");
        return -1;
    }
    return Promise.reject(error);
});

instance.interceptors.request