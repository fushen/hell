import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import * as ElementPlus from 'element-plus';
import { instance } from './plugins';
import qs from 'qs';
import G2 from '@antv/g2';

// 路由守卫
router.beforeEach((to, from, next) => {
  if(to.path !== "/login"){
    if(!store.state.isLogin){
        ElementPlus.ElMessage.error("登录状态过期，请重新登录!");
        next("/login");
    }else{
        next();
    }
  }else{
      // 登录过后，无法再次进入登录页
      if(store.state.isLogin){
        next("/home");
      }else{
        next();
      }
  }
});

const app = createApp(App);

app.config.globalProperties.$http = app.config.globalProperties.http = instance;
app.config.globalProperties.$qs = app.config.globalProperties.qs = qs;
app.config.globalProperties.G2 = G2;

app.use(store);
app.use(router);
app.use(ElementPlus);
app.mount('#app');
