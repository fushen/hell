import { createStore } from 'vuex';
import vuexPersist from 'vuex-persist';

export default createStore({
  state: {
    isLogin: false
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  },
    plugins: [new vuexPersist({ storage: window.sessionStorage }).plugin]
});
